/** * 
 * Peaks testi feilimisel screeshoti tegema.
 * **/

package com.nortal.course.selenium;

import java.io.File;
import java.io.FileOutputStream;

import org.junit.rules.MethodRule;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class FailedTestScreenshotRule implements MethodRule {

	@Override
	public Statement apply(final Statement base, final FrameworkMethod method, Object target) {
		return new Statement() {
			
			@Override
			public void evaluate() throws Throwable {
				try {
					base.evaluate();
				} catch (Throwable t) {
					captureScreeshot(method.getName());
					throw t; // rethrow to allow the failure to be reported to JUnit
				}
				
			}

			private void captureScreeshot(String fileName) {
				try {
					new File("reports").mkdirs();
					FileOutputStream out = new FileOutputStream(
							"reports/screenshot" + fileName + ".png");
					out.write(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES));
					
					out.close();
					
				} catch (Exception e) {
					// No need to crash the tests if the screenshot fails
				}
				
			}
		};
	}

}
